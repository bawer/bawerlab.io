+++
author = "Baver Ateş"
date = 2020-11-04T00:00:00Z
description = "Baver Ateş - Hakkında"
title = "Hakkımda"

+++
Instagram: [@baweratesv](https://www.instagram.com/baweratesv/)

Vsco: [@bawerw](https://vsco.co/bawerw/gallery)

GitLab: [@bawer](https://gitlab.com/bawer/)

***

Mail: [baver@tuta.io](mailto:baver@tuta.io)