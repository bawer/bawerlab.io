+++
author = "Baver Ateş"
date = 2020-11-04T00:00:00Z
description = "Baver Ateş - Telif Hakkı"
title = "Baver Ateş, Kişisel Web Sitesi İçerik Telif Hakkı"

+++
## [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode)

Bu, insan tarafından okunabilen özet [lisanstır](https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode) (lisansın tamamı yerine geçmez)

### Bunları yapmakta özgürsünüz:

* **Paylaş** — eseri her ortam veya formatta kopyalayabilir ve yeniden dağıtabilirsiniz.
* **Uyarla** — karıştır, aktar ve mevcut eserin üzerine inşa et

Lisans şartlarını yerine getirdiğiniz sürece, lisans sahibi bu özgürlükleri (belirtilen hakları) iptal edemez.

### Aşağıdaki şartlar altında:

* **Atıf** — uygun referans vermeli, lisansa bağlantı sağlamalı ve değişiklik yapıldıysa bilgi vermelisiniz. Bunları uygun bir şekilde yerine getirebilirsiniz fakat bu, lisans sahibinin sizi ve kullanım şeklinizi onayladığını göstermez.
* **GayriTicari** — Bu materyali ticari amaçlarla kullanamazsınız.
* **AynıLisanslaPaylaş** — Eğer materyali karıştırdınız, aktardınız ya da materyalin üzerine inşa ettiyseniz, ancak orjinal lisansın aynısı lisans ile dağıtabilirsiniz.


* **Ek sınırlamalar yoktur** — Lisansın sağladığı izinlerin kullanımını kanunen kısıtlayacak yasal koşullar ya da teknolojik önlemler uygulayamazsınız.